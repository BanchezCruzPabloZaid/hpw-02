/*
Un alumno esta formado por:
Número de control.
Nombre completo.
Un grupo(clave).

Un grupo esta compuesto por:
Una clave 
Un nombre
Una colección de Alumnos(claves)
Una colección de materias ---> deben ser 7 
Una colección de profesores (claves)
Una colección de horarios (claves)

Un Horario esta compuesto de:
Una clave
Una hora inicial.
Una hora final.
Una Materia.(clave)
Un profesor.(clave)
¿Un grupo(clave)?

Una materia esta compuesta por:
Una clave 
Un nombre

Un profesor está compuesto por:
Una clave 
Un nombre completo */

/*Forma casi Correcta*/
objalumno= {"NoControl":"09161133","Nombre":"Pablo Zaid Banchez", "grupo":"A"};

objgrupo= {"grupos":[{"clave":"GRP0987","alumnos":[{"clave":"09161133"},{"clave":"09161134"},{"clave":"09161135"},{"clave":"09161133"}],"Materias":["Español","Mat","His","Geo",
"CN","Fi","Qu"],"Profesores":["FGH6789","PSC8975","RAG87798","RMU0976","WER7890","KIO987"],"Horarios":["7-8Mat","8-9Es","9-10Hi","10-11Ge","11-12CN","12-13Fi","13-14Qu"]}]};

objHorario = {"Horarios":[{"clave":"7-8Mat","hora_i":"7:00","hora_f":"8:00","Materia":"Matematicas","Profesor":"FGH6789","grupos":"GRP0987"}]};

objmateria ={ "materias": [{"clave":"Esp","Nombre": "Español"}]}

objprofesor= {"profesores":[{"clave":"FGH6789","nombre":{"nombre":"Fernando","apellidos":{"Paterno":"Gutierrez","Materno":"Hernandez"}}}]}

grupos = {
    "grupo":objgrupo,
    "alumnos":objalumno,
    "materias":objmateria,
    "profesores":objprofesor,
    "horarios":objHorario
}

/*Crear una función que dado un grupo imprima una lista de alumnos*/
function imprimir_lista_alumnos(grupo)
{
	if (grupo["alumnos"])
	{
		for (i=0; i < grupo["alumnos"].length; i++)
		{
			console.log(obj_grupo["alumnos"][i]["Nombre"]);
		}
	}
}

imprimir_lista_alumnos(grupos);

/*Crear una función que dado un grupo imprime las relaciones de horario_lista_Materia*/

/*--------------------------------------------------------------------------------------------------------------*/
/*Profesor*/
obj_profesor =
[
	{
		"clave":"HBA896",
		"nombre":"Hernandez Blas Antonio"
	}
];

/*Materias*/
obj_materias = [
	{
		"clave":"m1",
		"nombre":"Matematicas 1"
	},
	{
		"clave":"m2",
		"nombre":"Español"
	},
	{
		"clave":"m3",
		"nombre":"Historia"
	},
	{
		"clave":"m4",
		"nombre":"Geografia"
	},
	{
		"clave":"m5",
		"nombre":"Civismo"
	},
	{
		"clave":"m6",
		"nombre":"Biologia"
	},
	{
		"clave":"m7",
		"nombre":"Educacion fisica"
	}
];

/*Alumnos*/
obj_alumnos = 
[
	{	
		"ncontrol":"09161133",
		"nombre":"Banchez Cruz Pablo Zaid",
		"grupo": "A"
	},
	{
		"ncontrol":"09161278",
		"nombre":"Gema Aidé Ruiz Avendaño",
		"grupo": "A"
	},
	{
		"ncontrol":"09161135",
		"nombre":"Luis Garcia Postiga",
		"grupo": "A"
	}
];

/*Horarios*/
obj_horarios =
[
	{
		"clave":"h1",
		"hora_inicial":"12:00",
		"hora_final":"13:00",
		"materia":obj_materia1["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	},
	{
		"clave":"h2",
		"hora_inicial":"13:00",
		"hora_final":"14:00",
		"materia":obj_materia2["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	},
	{
		"clave":"h3",
		"hora_inicial":"14:00",
		"hora_final":"15:00",
		"materia":obj_materia3["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	},
	{
		"clave":"h4",
		"hora_inicial":"15:00",
		"hora_final":"16:00",
		"materia":obj_materia4["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	},
	{
		"clave":"h5",
		"hora_inicial":"16:00",
		"hora_final":"17:00",
		"materia":obj_materia5["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	},
	{
		"clave":"h6",
		"hora_inicial":"17:00",
		"hora_final":"18:00",
		"materia":obj_materia6["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	},
	{
		"clave":"h7",
		"hora_inicial":"19:00",
		"hora_final":"20:00",
		"materia":obj_materia7["clave"],
		"profesor":obj_profesor["clave"],
		"grupo": "A"
	}
];

/*Grupo*/
obj_grupo =
{
	"clave":"g1",
	"nombre":"A",
	"alumnos": obj_alumnos,
	"materias": obj_materias,
	"profesores": obj_profesor,
	//"horarios": obj_horarios
};

/*Imprimit lista de alumnos en el grupo*/
function imprimir_lista_alumnos(grupo)
{
	if (grupo["alumnos"])
	{
		for (i=0; i < grupo["alumnos"].length; i++)
		{
			console.log(obj_grupo["alumnos"][i]["nombre"]);
		}
	}
}

imprimir_lista_alumnos(obj_grupo);


/*--------------------------------------------------------------------------------------------------------------*/




/*Segunda forma*/

objgrupo1= {"clave":"GRP0987","alumnos":[{"clave":"09161133"},{"clave":"09161134"},{"clave":"09161135"},{"clave":"09161133"}],"Materias":[{"Esp":{"clave":"Esp","Nombre": "Español"}},"Mat","His","Geo",
"CN","Fi","Qu"],"Profesores":[{"FGH6789":{"clave":"FGH6789","nombre":{"nombre":"Fernando","apellidos":{"Paterno":"Gutierrez","Materno":"Hernandez"}}}},"PSC8975","RAG87798","RMU0976","WER7890","KIO987"],"Horarios":[{"7-8Mat":{"clave":"7-8Mat","hora_i":"7:00","hora_f":"8:00","Materia":"Matematicas","Profesor":"FGH6789","grupos":"GRP0987"}},"8-9Es","9-10Hi","10-11Ge","11-12CN","12-13Fi","13-14Qu"]};


/*Tercera forma*/

grupo ={
    "clave": "12345",
    "nombre": "isa",
    "alumnos": [
        {"NoControl":"09161278", "nombre":"Banchez Cruz Pablo"},
        {"NoControl":"09161278", "nombre":"Banchez Cruz Pablo"},
        {"NoControl":"09161278", "nombre":"Banchez Cruz Pablo"},
       
        ],
    "materias": [{"clave": "ISA123","nombre": "matematicas"},
                {"clave": "ISA124","nombre": "ESPAÑOL"},
                {"clave": "ISA125","nombre": "GEOGRAFIA"},
                {"clave": "ISA126","nombre": "FISICA"},
                {"clave": "ISA127","nombre": "QUIMICA"},
                {"clave": "ISA128","nombre": "ORIENTACION EDUCATIVA"},
                {"clave": "ISA129","nombre": "ETICA"}
        ],

    "profesores": [ {"CLAVE":"AHB123", "NOMBRE": "ANTONIO HERNANDEZ BLAS"}],
    
    
   "horario": [{"clave": "isa33","hora_ini": "8:00","hora_fin": "9:00","materia": "ISA123","profesor": "AHB123","grupo": "12345"} ]}



/*----------------------------------------------------------------------------------------------------------*/


function alum_grupos(alumnos){
    if(alumnos["alumnos"]){
        return alumnos["alumnos"];
    }
    return null;
}

console.log(alum_grupos(objgrupo1));




